<?php namespace Lordcoste\Cronjob;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\HTML;
use Orchestra\Html\Table\TableBuilder;
use Orchestra\Model\Role as Eloquent;
use Orchestra\Support\Facades\Form;
use Orchestra\Support\Facades\Table;

class CronjobPresenter extends \Orchestra\Control\Presenter\AbstractablePresenter
{
    /**
     * View form generator for Orchestra\Model\Role.
     *
     * @param  \Orchestra\Model\Role    $model
     * @return \Orchestra\Html\Form\FormBuilder
     */
    public function form($job)
    {
        return Form::of('control.cronjob', function ($form) use ($job) {

            $url = 'control.cronjob';

            if ($job != null) {
                $url .= '/' . $job->id;

                $form->hidden('_method', function ($control) {
                    $control->value('PUT');
                });

                $form->hidden('id', function ($control) use ($job) {
                    $control->value($job->id);
                });
            }

            $form->setup($this, $url, $job);

            $form->fieldset(function ($fieldset) {
                $fieldset->control('input:text', 'minutes', function ($control) {
                    $control->label('Minutes');
                });
                $fieldset->control('input:text', 'hours', function ($control) {
                    $control->label('Hours');
                });
                $fieldset->control('input:text', 'dayofmonth', function ($control) {
                    $control->label('Day of Month');
                });
                $fieldset->control('input:text', 'months', function ($control) {
                    $control->label('Month');
                });
                $fieldset->control('input:text', 'dayofweek', function ($control) {
                    $control->label('Day of Week');
                });
                $fieldset->control('input:text', 'task', function ($control) {
                    $control->label('Task');
                });
            });
        });
    }
}
