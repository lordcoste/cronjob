<?php

namespace Lordcoste\Cronjob;

use TiBeN\CrontabManager\CrontabRepository;
use TiBeN\CrontabManager\CrontabAdapter;
use TiBeN\CrontabManager\CrontabJob;

class CronjobRepository
{
    /**
    *
    * @var Singleton
    */
    private static $instance;

    private static $repository;

    private function __construct()
    {
        self::$repository = new CrontabRepository(new CrontabAdapter());
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public static function getJobs()
    {
        try {
            return self::$repository->getJobs();
        } catch (\DomainException $e) {
            return array();
        }
    }

    public static function getByHash($hash)
    {
        $cronjobs = self::getJobs();

        if (!empty($cronjobs)) {
            foreach ($cronjobs as $job) {
                if ($hash == md5(json_encode($job))) {
                    return $job;
                }
            }
        }

        return null;
    }

    public static function addJob($crontab)
    {
        $job = CrontabJob::createFromCrontabLine($crontab);

        self::$repository->addJob($job);
        self::$repository->persist();

        return $job;
    }

    public static function updateJob(CrontabJob $job)
    {
        self::$repository->persist();

        return $job;
    }

    public static function deleteJobByHash($hash)
    {
        $job = self::getByHash($hash);

        if ($job == null) {
            return false;
        }

        self::$repository->removeJob($job);
        self::$repository->persist();

        return true;
    }
}
