@include('orchestra/control::widgets.menu')

<? Orchestra\Support\Facades\Site::set('header::add-button', true); ?>

<div class="row">
    <div class="twelve columns white rounded box">
        <table class="table table-striped">
            <thead>
                <tr>
                    <!--<th>Enabled</th>-->
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Command</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @if (empty($cronjobs))
                    <tr>
                        <td colspan="7">No cronjobs</td>
                    </tr>
                @else
                    @foreach ($cronjobs as $cronjob)
                    <tr>
                        <!--<td><?php echo $cronjob->enabled; ?></td>-->
                        <td><?php echo $cronjob->minutes; ?></td>
                        <td><?php echo $cronjob->hours; ?></td>
                        <td><?php echo $cronjob->dayOfMonth; ?></td>
                        <td><?php echo $cronjob->months; ?></td>
                        <td><?php echo $cronjob->dayOfWeek; ?></td>
                        <td><?php echo $cronjob->taskCommandLine; ?></td>
                        <td>
                            <div class="btn-group">
                                <a href="<?php echo handles('orchestra/foundation::resources/control.cronjob/' . md5(json_encode($cronjob)) . '/edit'); ?>"
                                    class="btn btn-mini btn-warning">Edit</a>
                                <a href="<?php echo handles('orchestra/foundation::resources/control.cronjob/' . md5(json_encode($cronjob)) . '/delete'); ?>"
                                    class="btn btn-mini btn-danger">Delete</a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>