<?php

namespace Lordcoste\Cronjob;

use Lordcoste\Cronjob\Cronjob as CronjobProcessor;

use Orchestra\App;
use Orchestra\Form;
use Orchestra\Messages;
use Orchestra\Site;
use Orchestra\Support\Facades\Facile;

use TiBeN\CrontabManager\CrontabJob;

class CronjobController extends \Orchestra\Control\Routing\BaseController
{
    /**
     * Setup a new controller.
     *
     * @param  \Orchestra\Control\Processor\Acl    $processor
     */
    public function __construct(CronjobProcessor $processor)
    {
        $this->processor = $processor;

        parent::__construct();
    }

    /**
     * Define the filters.
     *
     * @return void
     */
    protected function setupFilters()
    {
        $this->beforeFilter('control.manage:orchestra');
    }

    public function index()
    {
        return $this->processor->index($this);
    }

    public function delete($hash)
    {
        return $this->processor->delete($this, $hash);
    }

    public function create()
    {
        return $this->processor->create($this);
    }

    public function edit($hash)
    {
        return $this->processor->edit($this, $hash);
    }

    /**
     * Create the cronjob.
     *
     * @return Response
     */
    public function store()
    {
        return $this->processor->store($this, \Input::all());
    }

    /**
     * Update the cronjob.
     *
     * @param  string  $hash
     * @return Response
     */
    public function update($hash)
    {
        return $this->processor->update($this, \Input::all(), $hash);
    }

    /**
     * Response when list page succeed.
     *
     * @param  array  $data
     * @return Response
     */
    public function indexSucceed(array $data)
    {
        Site::set('title', 'Active Cronjob');

        return \View::make('lordcoste/cronjob::index', $data);
    }

    /**
     * Response when destroy succeed.
     *
     * @return Response
     */
    public function destroySucceed()
    {
        $message = 'Cronjob deleted.';

        return $this->redirectWithMessage(resources('control.cronjob'), $message);
    }

    /**
     * Response when deleting failed.
     *
     * @return Response
     */
    public function destroyFailed()
    {
        $message = 'Could not delete the specified cronjob.';

        return $this->redirectWithMessage(resources('control.cronjob'), $message, 'error');
    }

    /**
     * Response when create succeed.
     *
     * @param  array  $data
     * @return Response
     */
    public function createSucceed(array $data)
    {
        Site::set('title', 'Add cronjob');

        return \View::make('lordcoste/cronjob::edit', $data);
    }

    /**
     * Response when storing cronjob failed on validation.
     *
     * @param  object  $validation
     * @return Response
     */
    public function storeValidationFailed($validation)
    {
        Messages::add('error', $validation->errors()->first('crontab'));
        return $this->redirectWithErrors(resources('control.cronjob/create'), $validation);
    }

    /**
     * Response when storing cronjob failed.
     *
     * @param  array    $error
     * @return Response
     */
    public function storeFailed($error)
    {
        return $this->redirectWithMessage(resources('control.cronjob'), $error, 'error');
    }

    /**
     * Response when storing user succeed.
     *
     * @param  \TiBeN\CrontabManager\CrontabJob   $cronjob
     * @return Response
     */
    public function storeSucceed(CrontabJob $job)
    {
        $message = 'Cronjob successfully created';

        return $this->redirectWithMessage(resources('control.cronjob'), $message);
    }

    /**
     * Response when updating cronjob failed on validation.
     *
     * @param  object  $validation
     * @param  string $hash
     * @return Response
     */
    public function updateValidationFailed($validation, $hash)
    {
        Messages::add('error', $validation->errors()->first('crontab'));
        return $this->redirectWithErrors(resources("control.cronjob/{$hash}/edit"), $validation);
    }

    /**
     * Response when updating cronjob failed.
     *
     * @param  array   $error
     * @return Response
     */
    public function updateFailed(array $error)
    {
        return $this->redirectWithMessage(resources('control.cronjob'), $error, 'error');
    }

    /**
     * Response when updating cronjob succeed.
     *
     * @param  CrontabJob   $cronjob
     * @return Response
     */
    public function updateSucceed(CrontabJob $cronjob)
    {
        $message = 'Cronjob successfully updated';

        return $this->redirectWithMessage(resources('control.cronjob'), $message);
    }

    /**
     * Response when edit cronjob page succeed.
     *
     * @param  array  $data
     * @return Response
     */
    public function editSucceed(array $data)
    {
        Site::set('title', 'Edit cronjob');

        return \View::make('lordcoste/cronjob::edit', $data);
    }
}
