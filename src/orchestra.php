<?php

use Orchestra\Support\Facades\Resources;

View::addNamespace('lordcoste/cronjob', __DIR__.'/views');

/*
|--------------------------------------------------------------------------
| Backend Routing
|--------------------------------------------------------------------------
*/
Event::listen('orchestra.started: admin', function () {

    $control = Resources::of('control');

    $control['cronjob'] = 'resource:Lordcoste\Cronjob\CronjobController';
});

View::composer('orchestra/control::widgets.menu', function ($view) {
    $view->setPath(base_path() . '/vendor/lordcoste/cronjob/src/views/menu.blade.php');
});
