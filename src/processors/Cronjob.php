<?php
namespace Lordcoste\Cronjob;

use Orchestra\Support\Facades\App;
use Orchestra\Support\Facades\Form;

class Cronjob extends \Orchestra\Control\Processor\AbstractableProcessor
{

    /**
     * Setup a new processor instance.
     *
     * @param  CronjobPresenter   $presenter
     * @param  CronjobValidator  $validator
     */
    public function __construct(CronjobPresenter $presenter, CronjobValidator $validator)
    {
        $this->presenter = $presenter;
        $this->validator = $validator;
    }

    /**
     * List available cronjob.
     *
     * @param  object  $listener
     * @param  string  $type
     * @return mixed
     */
    public function index($listener)
    {
        $cronjobs = CronjobRepository::getInstance()->getJobs();

        return $listener->indexSucceed(compact('cronjobs'));
    }

    public function delete($listener, $hash)
    {
        if (CronjobRepository::getInstance()->deleteJobByHash($hash)) {
            return $listener->destroySucceed();
        }

        return $listener->destroyFailed();
    }

    /**
     * View create a cronjob page.
     *
     * @param  object  $listener
     * @return mixed
     */
    public function create($listener)
    {
        $form = $this->presenter->form(null);

        return $listener->createSucceed(compact('form'));
    }

    /**
     * View edit a cronjob page.
     *
     * @param  object          $listener
     * @param  string|integer  $id
     * @return mixed
     */
    public function edit($listener, $hash)
    {
        $job = CronjobRepository::getInstance()->getByHash($hash);

        $job->id         = $hash;
        $job->dayofmonth = $job->dayOfMonth;
        $job->task       = $job->taskCommandLine;
        $job->dayofweek  = $job->dayOfWeek;

        $form = $this->presenter->form($job);

        return $listener->editSucceed(compact('form'));
    }

    /**
     * Store a cronjob.
     *
     * @param  object  $listener
     * @param  array   $input
     * @return mixed
     */
    public function store($listener, array $input)
    {
        $input['crontab'] = $input['minutes'] . ' ' . $input['hours'] . ' ' . $input['dayofmonth'] . ' ' . $input['months'] . ' ' . $input['dayofweek'] . ' ' . $input['task'];

        $validator = App::make('Lordcoste\Cronjob\CronjobValidator')->with($input);
        //$validator = \Validator::make($input, $rules);

        if ($validator->fails()) {
            return $listener->storeValidationFailed($validator);
        }

        try {
            $job = CronjobRepository::getInstance()->addJob($input['crontab']);
        } catch (\Exception $e) {
            return $listener->storeFailed($e->getMessage());
        }

        return $listener->storeSucceed($job);
    }

    /**
     * Update a role.
     *
     * @param  object  $listener
     * @param  array   $input
     * @param  string $hash
     * @return mixed
     */
    public function update($listener, array $input, $hash)
    {
        $job = CronjobRepository::getInstance()->getByHash($hash);

        $input['crontab'] = $input['minutes'] . ' ' . $input['hours'] . ' ' . $input['dayofmonth'] . ' ' . $input['months'] . ' ' . $input['dayofweek'] . ' ' . $input['task'];

        $validator = App::make('Lordcoste\Cronjob\CronjobValidator')->with($input);

        if ($validator->fails()) {
            return $listener->updateValidationFailed($validator, $hash);
        }

        $job->minutes         = $input['minutes'];
        $job->hours           = $input['hours'];
        $job->dayOfMonth      = $input['dayofmonth'];
        $job->months          = $input['months'];
        $job->dayOfWeek       = $input['dayofweek'];
        $job->taskCommandLine = $input['task'];

        try {
            $job = CronjobRepository::getInstance()->updateJob($job);
        } catch (Exception $e) {
            return $listener->updateFailed($e->getMessage());
        }

        return $listener->updateSucceed($job);
    }
}
