<?php namespace Lordcoste\Cronjob;

use Orchestra\Support\Validator;

class CronjobValidator extends Validator
{
    public function __construct()
    {
        $this->rules = [
            'minutes'    => ['required'],
            'hours'      => ['required'],
            'dayofmonth' => ['required'],
            'months'     => ['required'],
            'dayofweek'  => ['required'],
            'task'       => ['required'],
            'crontab'    => ['required', 'regex:/' . $this->crontabRegex().'/'],
        ];
    }

    /**
     * List of rules
     *
     * @var array
     */
    protected $rules = [];

    private function crontabRegex()
    {
        $numbers = array(
            'min' => '[0-5]?\d',
            'hour' => '[01]?\d|2[0-3]',
            'day' => '0?[1-9]|[12]\d|3[01]',
            'month' => '[1-9]|1[012]',
            'dow' => '[0-6]'
        );

        foreach ($numbers as $field => $number) {
            $range = "(?:$number)(?:-(?:$number)(?:\/\d+)?)?";
            $field_re[$field] = "\*(?:\/\d+)?|$range(?:,$range)*";
        }

        $field_re['month'].='|jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec';
        $field_re['dow'].='|mon|tue|wed|thu|fri|sat|sun';

        $fields_re = '(' . join(')\s+(', $field_re) . ')';

        $replacements = '@reboot|@yearly|@annually|@monthly|@weekly|@daily|@midnight|@hourly';

        return '^\s*(' .
                '$' .
                '|#' .
                '|\w+\s*=' .
                "|$fields_re\s+" .
                "|($replacements)\s+" .
                ')' .
                '([^\\s]+)\\s+' .
                '(.*)$';
    }
}
